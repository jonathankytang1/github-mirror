import java.lang.reflect.Array;
import java.util.Arrays;

public class Exercise_3 {
    public static void main(String[] args) {
        String make, model;
        double engineSize;
        byte gear;

        make = "audi";
        model = "r8";
        engineSize = 23.93;
        gear = 2;

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);

        short speed = (short)(gear * 20);

        System.out.println("The speed is " + speed);

        if (engineSize <= 1.3) {
            System.out.println("Car is weak");
        } else {
            System.out.println("Car is powerful");
        }

        if (gear == 1) {
            System.out.println("Speed is <10");
        } else if (gear == 2) {
            System.out.println("Speed is 10-20");
        } else if (gear == 3) {
            System.out.println("Speed is 20-30");
        } else if (gear == 4) {
            System.out.println("Speed is 30-40");
        } else if (gear == 5) {
            System.out.println("Speed is 40+");
        }

        int leapYears = 0;

        for (int i = 1900; i <= 2000; i++) {
            if (leapYears >= 5) {
                break;
            }
            if (i % 4 == 0 || i % 400 == 0){
                System.out.println(i + " is a leap year.");
                leapYears++;
            }
        }

        switch (gear) {
            case 1:
                System.out.println("Speed is <10");
                break;

            case 2:
                System.out.println("Speed is 10-20");
                break;

            case 3:
                System.out.println("Speed is 20-30");
                break;

            case 4:
                System.out.println("Speed is 30-40");
                break;

            case 5:
                System.out.println("Speed is 40+");
                break;
        }

        int[] list = {10,20,30,40,50,60,70,80,90,100};

        leapYears = 0;

        for (int i = 1900; i <= 2000; i++) {
            if (leapYears >= 10) {
                break;
            }
            if (i % 4 == 0 || i % 400 == 0) {
                list[leapYears] = i;
                leapYears++;
            }
        }
        System.out.println(Arrays.toString(list));
    }
}
