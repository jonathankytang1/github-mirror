import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestStrings {
    public static void main(String[] args) {
        // 2
        StringBuilder fileName = new StringBuilder("example.doc");
        fileName.replace(fileName.length()-3, fileName.length(), "bak");

        System.out.println(fileName);

        // 3
        String name1 = "Ab";
        String name2 = "to";

        if (name1.equals(name2)) {
            System.out.println("The same!");
        }

        String greater = (name1.compareTo(name2) > 0 ) ? name1 : name2;

        System.out.println("The greater string is " + greater);

        String text = "the quick brown fox swallowed down the lazy chicken";

        // 4
        int count = 0;

        for (int i = 0; i < text.length()-2; ++i) {
            if (text.substring(i, i+2).equals("ow")) {
                count++;
            }
        }
        System.out.printf("the text ow appears %d times\n", count);

        // 5
        StringBuilder pal = new StringBuilder("Linenotonevil");
        StringBuilder palCopy = new StringBuilder(pal);
        if (pal.reverse().toString().equalsIgnoreCase(palCopy.toString())) {
            System.out.println("Its a palindrome");
        }

        // 6
        Format yearOnly = new SimpleDateFormat("yyyy");
        Format dayMonthYear = new SimpleDateFormat("dd MM yyyy");
        Format time = new SimpleDateFormat("hh mm");
        System.out.println(yearOnly.format(new Date()));
        System.out.println(dayMonthYear.format(new Date()));
        System.out.println(time.format(new Date()));
    }
}
