public class HomeInsurance implements Detailable {
    private double premium, excess, amountInsured;

    public HomeInsurance(double premium, double excess, double amountInsured) {
        this.premium = premium;
        this.excess = excess;
        this.amountInsured = amountInsured;
    }

    public String getDetails() {
        return "Excess is " + excess + "Amount insured is " + amountInsured + " Premium is" + premium;
    }

}