public abstract class Account implements Detailable {
    private double balance;
    private String name;

    public static double interestRate = 1.5;

    public Account(String s, double d)
    {
        name = s;
        balance = d;
    }

    public Account()
    {
        // constructor calling another constructor in the same class
        this("JT", 500);
    }

    public void setBalance(double b) {
        balance = b;
    }

    public double getBalance() {
        return balance;
    }

    public void setName(String n) {
        name = n;
    }

    public String getName() {
        return name;
    }

    public abstract void addInterest();
}
