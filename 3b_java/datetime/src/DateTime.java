import java.sql.Time;
import java.text.ParseException;
import java.time.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateTime {
    public static void main(String[] args) throws ParseException {
        // q1
        LocalDate today = LocalDate.now();
        LocalDate bday = LocalDate.of(2022, Month.FEBRUARY, 26);

        Period gap = Period.between(bday, today);
        System.out.println("Period is: " + gap);

        // q2
        Date london = new Date();

        DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:SSz");
        df.setTimeZone(TimeZone.getTimeZone("Europe/London"));

        String lon = df.format(london);
        System.out.println("Date in BST: " + lon);

        df.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        String nyc = df.format(london);
        System.out.println("Date in EST: " + nyc);

        // q3
        Date dateStart = new Date();
        String dateEnd = "10/6/2021 17:00:00";

        SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        Date d2 = null;
        d2 = format.parse(dateEnd);

        long diff = d2.getTime() - dateStart.getTime();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

        System.out.println("Minutes until the end: " + diff);

    }
}
