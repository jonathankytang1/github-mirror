package com.training.northwind.repository;

import com.training.northwind.entities.Shipper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ShipperRepository extends JpaRepository<Shipper, Long> {
}
