package com.training.northwind.controller;

import com.training.northwind.entities.Shipper;
import com.training.northwind.service.ShipperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/shipper")
public class ShipperController {

    @Autowired
    private ShipperService shipperService;

    @GetMapping
    public List<Shipper> findAll() {
        return shipperService.findAll();
    }
}
