use northwind;
show indexes from orders;
select companyname, orderid, orderdate
	from customers c, orders o
		where c.customerid = o.customerid and c.country = 'Germany';
        
select companyname, orderid, orderdate, CONCAT(FirstName,' ',LastName) 'Sales Person'
	from customers c, orders o
		where c.customerid = o.customerid and c.country = 'Germany';

SELECT companyname, orderid, orderdate, CONCAT(FirstName,' ',LastName) 'Sales Person'
	from customers c 
    JOIN orders o ON c.customerid = o.customerid 
    JOIN Employees e on o.EmployeeID = e.EmployeeID
    WHERE c.country = 'Germany';
    
# (LEFT or RIGHT) OUTER JOIN
SELECT s.CompanyName Supplier, s.Country 'Supplier Country', c.CompanyName Customer, c.Country 'Customer Country'
FROM suppliers s LEFT JOIN Customers c ON s.Country = c.Country;

# CROSS JOIN
SELECT CategoryName, ProductName FROM Categories CROSS JOIN Products ORDER BY CategoryName;

select * from suppliers;

select e.LastName Employee, e.title, m.LastName Manager 
	from employees e left join Employees m
		on m.EmployeeID = e.ReportsTo;
        
-- Unions 
select CompanyName, ContactName from Customers
UNION
select CompanyName, ContactName from Suppliers;

-- LIMIT
select ProductName, UnitPrice from Products
order by UnitPrice DESC limit 5;

-- SUM, AVG, MIN, MAX, COUNT (NOT SQRT)
select sum(Quantity) 'TotalSales',
		count(Quantity) 'NumSales',
        avg(Quantity) 'AverageSales',
        min(Quantity) 'MinSale',
        max(Quantity) 'MaxSale'
        from order_details;

-- top 10 most expensive products
select ProductName, UnitPrice from Products order by UnitPrice DESC LIMIT 10;

select Region from Customers;
select count(*) from customers;
select count(region) from customers;

select count(distinct region) from customers;

-- group by/having
select ProductName, sum(Quantity) 'Total Sales' from order_details od
join products p on od.productID = p.productID group by ProductName;

/*Write a SQL Statement that displays the revenue for each category. The revenue
can be calculated using the expression (unitprice * quantity) * (1 – discount). You
will need to join the Categories, Products and Order_Details tables to do this.*/

select CategoryName, sum((o.UnitPrice * o.Quantity) * (1-o.Discount)) 'Total Revenue'
from categories c join products p on c.categoryID = p.categoryID
join order_details o on o.productID = p.productID group by categoryname
having sum(Quantity) >= 1000;

select ProductName, sum(Quantity) 'Total Sales' from order_details od
join products p on od.productid = p.productid
group by productname
having sum(quantity) >= 1000;

-- Rollup operator
select CategoryName, ProductName, SUM(Quantity) as Sales, FORMAT(SUM(od.UnitPrice * Quantity),2) as Revenue
from products p join order_details od
on p.ProductID = od.ProductID join categories c
on c.categoryID = p.CategoryID
group by CategoryName, ProductName
with rollup;

-- sub-queries
select CompanyName, Country from Customers 
where country in ( select distinct Country from employees);

SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, Quantity 
FROM Products p JOIN Order_Details od ON p.ProductID = od.ProductID 
JOIN Orders o ON o.OrderID = od.OrderID 
WHERE Quantity = (SELECT MAX(Quantity) FROM order_details od1 WHERE od1.productID = p.productID)
GROUP BY ProductName
ORDER BY ProductName, Quantity DESC;

