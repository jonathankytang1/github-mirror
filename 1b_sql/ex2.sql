select CategoryName, CompanyName, ProductName, UnitPrice
from products p
join categories c on c.categoryid = p.categoryid
join suppliers s on p.supplierid = s.supplierid;