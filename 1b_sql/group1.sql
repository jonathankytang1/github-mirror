use sakila;

-- question 1
select First_Name, Last_Name, Email from Customer;

-- question 2
select distinct city from city;

-- question 3
select * from Customer where Last_name = 'Lee';

-- question 4
select * from Customer where Last_name like 'M%';

-- question 5
INSERT INTO Customer(store_id,first_name,last_name,email,address_id) VALUES(1,"DAVID","BOWMAN","DAVID.BOWMAN@sakilacustomer.org",252);

select first_name, last_name, sum(amount) as total_amount
from customer c
inner join payment p on c.customer_id = p.customer_id
group by first_name, last_name;

-- question 6
select first_name, last_name, sum(amount) as total_amount
from customer c
inner join payment p on c.customer_id = p.customer_id
where amount<>0
group by first_name, last_name;

-- question 7
select first_name, last_name, sum(amount) as total_amount
from customer c
inner join payment p on c.customer_id = p.customer_id
where amount<>0
group by first_name, last_name
order by total_amount desc;
