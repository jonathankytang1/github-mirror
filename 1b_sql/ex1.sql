CREATE DATABASE CourseDB;
USE CourseDB;
SHOW DATABASES;

CREATE TABLE Suppliers (
	SupplierID INT auto_increment primary key,
    Name VARCHAR(40),
    Country VARCHAR(40),
    City VARCHAR(40)
);

CREATE TABLE Products (
	ProductID INT,
    Name VARCHAR(50),
    Price DECIMAL(6,2),
    constraint pk_product PRIMARY KEY (ProductID),
    foreign key (ProductID) REFERENCES Suppliers(SupplierID)
);

